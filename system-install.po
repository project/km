# translation of system-install.po to Khmer
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2007.
# អេង វណ្ណៈ <evannak@khmeros.info>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: system-install\n"
"POT-Creation-Date: 2007-01-16 11:35+0100\n"
"PO-Revision-Date: 2007-03-15 15:05+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: KBabel 1.11.4\n"

#: modules/system/system.install:104
msgid "You can <a href=\"@cron\">run cron manually</a>."
msgstr "អ្នក​អាច <a href=\"@cron\">រត់ cron ដោយ​ដៃ</a> ។"

#: modules/system/system.install:3245
msgid "If you want to add a static page, like a contact page or an about page, use a page."
msgstr "បើ​អ្នក​ចង់​បន្ថែម​ទំព័រ​ថិតិវន្ត ដូច​ជា​ទំព័រ​ទំនាក់ទំនង ឬ​ទំព័រ​អំពី ប្រើ​ទំព័រមួយ ។"

#: modules/system/system.install:3252
msgid "Story"
msgstr "រឿងរ៉ាវ"

#: modules/system/system.install:3254
msgid "Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles."
msgstr "រឿងរ៉ាវ​គឺ​ជា​អត្ថបទ​នៅ​ក្នុង​សំណុំ​បែបបទ​សាមញ្ញា​បំផុត​របស់​ពួកវា ៖ ពួក​វា​មាន​ចំណង​ជើង សំណួរ និង​តួ ប៉ុន្តែ​អាច​ត្រូវ​បាន​ពង្រីក​ដោយ​ម៉ូឌុល​ផ្សេង​ទៀត ។  សំណួរ​គឺ​ជា​ផ្នែក​មួយ​របស់​តួ​ដែរ ។ រឿងរ៉ាវ​អាច​ត្រូវ​បាន​ប្រើ​ជា​កំណត់​ហេតុ​បណ្ដាញ​ផ្ទាល់​ខ្លួន ឬ​សម្រាប់​​អត្ថបទ​ថ្មីៗ ។"

#: modules/system/system.install:3354
msgid "URL Filter module was disabled; this functionality has now been added to core."
msgstr "ម៉ូឌុល​តម្រង URL ត្រូវ​បានបិទ មុខងារ​នេះ​ឥឡូវត្រូវ​បានបន្ថែម​ទៅចំណុច​សំខាន់ ។"

#: modules/system/system.install:30
msgid "Web server"
msgstr "ម៉ាស៊ីន​បម្រើ​​បណ្ដាញ"

#: modules/system/system.install:39
msgid "Your Apache server is too old. Drupal requires at least Apache %version."
msgstr "ម៉ាស៊ីន​បម្រើ Apache គឺ​ចាស់​ពេក ។ Drupal ត្រូវ​ការ​យ៉ាង​ហោចណាស់ Apache %version ។"

#: modules/system/system.install:45
msgid "The web server you're using has not been tested with Drupal and might not work properly."
msgstr "ម៉ាស៊ីន​បម្រើ​​បណ្ដាញ ដែល​អ្នក​កំពុង​ប្រើ ​មិន​ទាន់​បាន​សាកល្បង​ជាមួយ Drupal នៅ​ឡើយ​ទេ ហើយ​​ប្រហែល​ជា​មិន​ដំណើរ​ការ​ត្រឹមត្រូវ​ទេ ។"

#: modules/system/system.install:51
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: modules/system/system.install:52
msgid "Unable to determine your web server type and version. Drupal might not work properly."
msgstr "មិន​អាច​កំណត់​ប្រភេទ និង​កំណែ​ម៉ាស៊ីន​បម្រើ​​បណ្ដាញ​របស់​អ្នកបាន​ទេ ។ Drupal ប្រហែល​ជា​មិន​​ដំណើរការ​​ត្រឹមត្រូវ​ទេ ។"

#: modules/system/system.install:62
msgid "Your PHP installation is too old. Drupal requires at least PHP %version."
msgstr "ការ​ដំឡើង PHP របស់​អ្នក​ចាស់​ពេក ។ Drupal ទាមទារ​យ៉ាង​ហោច PHP %version ។"

#: modules/system/system.install:76
msgid "Not protected"
msgstr "មិន​បាន​ការពារ"

#: modules/system/system.install:78
msgid "The file %file is not protected from modifications and poses a security risk. You must change the file's permissions to be non-writable."
msgstr "ឯកសារ %file មិន​ត្រូវ​បាន​ការពារ​ពី​ការ​កែប្រែ និង​​បែបបទ​គ្រោះថ្នាក់​​ផ្នែក​សុវត្ថិភាព ។ អ្នក​ត្រូវ​តែ​ផ្លាស់ប្ដូរ​សិទ្ធិ​របស់​ឯកសារ​ទៅជា​​មិន​អាច​សរសេរ​បាន ។"

#: modules/system/system.install:83
msgid "Protected"
msgstr "បាន​ការពារ"

#: modules/system/system.install:86
msgid "Configuration file"
msgstr "ឯកសារ​​​កំណត់​រចនា​សម្ព័ន្ធ"

#: modules/system/system.install:94
msgid "Last run !time ago"
msgstr "រត់​ចុងក្រោយ !time កន្លង​ទៅ"

#: modules/system/system.install:98
msgid "Cron has not run. It appears cron jobs have not been setup on your system. Please check the help pages for <a href=\"@url\">configuring cron jobs</a>."
msgstr "Cron មិន​រត់​ទេ ។ វា​បង្ហាញ​ថាការងារ​របស់ cron មិន​ត្រូវ​បាន​រៀបចំ​នៅ​ក្នុង​ប្រព័ន្ធ​របស់​អ្នក​ទេ ។ សូម​ពិនិត្យ​មើល​ទំព័រ​ជំនួយ​សម្រាប់ <a href=\"@url\">ការ​កំណត់​រចនាសម្ព័ន្ធ​ការងារ​របស់ cron​</a> ។"

#: modules/system/system.install:100
msgid "Never run"
msgstr "កុំ​រត់"

#: modules/system/system.install:106
msgid "Cron maintenance tasks"
msgstr "ភារកិច្ច​ថែទាំ Cron"

#: modules/system/system.install:119
msgid "The directory %directory is not writable."
msgstr "ថត %directory មិន​អាច​សរសេរ​បាន ។"

#: modules/system/system.install:122
msgid "Not writable"
msgstr "មិន​អាច​សរសេរ​បាន"

#: modules/system/system.install:124
msgid "You may need to set the correct directory at the <a href=\"@admin-file-system\">file system settings page</a> or change the current directory's permissions so that it is writable."
msgstr "អ្នក​អាច​ត្រូវការ​កំណត់​ថត​ត្រឹមត្រូវ​នៅ <a href=\"@admin-file-system\">ទំព័រ​​កំណត់​ប្រព័ន្ធ​ឯកសារ</a> ឬ ​ផ្លាស់ប្ដូរ​សិទ្ធិ​របស់​ថត​បច្ចុប្បន្ន ដូច្នេះ​វា​អាច​សរសេរ​បាន ។"

#: modules/system/system.install:130
msgid "Writable (<em>public</em> download method)"
msgstr "អាច​សរសេរ​បាន (វិធី​សាស្ត្រ​ទាញយក <em>សាធារណៈ</em>)"

#: modules/system/system.install:135
msgid "Writable (<em>private</em> download method)"
msgstr "អាច​សរសេរ​បាន (វិធីសាស្ត្រ​ទាញយក <em>ឯកជន</em>)"

#: modules/system/system.install:145
msgid "Database schema"
msgstr "គ្រោងការណ៍​មូលដ្ឋាន​ទិន្នន័យ"

#: modules/system/system.install:147
msgid "Up to date"
msgstr "ទាន់សម័យ"

#: modules/system/system.install:157
msgid "Out of date"
msgstr "ហួស​សម័យ"

#: modules/system/system.install:158
msgid "Some modules have database schema updates to install. You should run the <a href=\"@update\">database update script</a> immediately."
msgstr "ម៉ូឌុល​មួយ​ចំនួន​​មាន​ភាព​ទាន់​សម័យ​គ្រោងការណ៍​មូលដ្ឋាន​ទិន្នន័យ​​ត្រូវ​​ដំឡើង ។ អ្នក​គួរតែ​​រត់ <a href=\"@update\">ស្គ្រីប​ធ្វើ​ឲ្យ​ទាន់​សម័យ​មូលដ្ឋាន​ទិន្នន័យ</a> ភ្លាមៗ ។"

